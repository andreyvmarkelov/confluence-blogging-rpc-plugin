package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelPermissionSupport;
import com.atlassian.confluence.labels.persistence.dao.LabelSearchResult;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.NotPermittedException;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Vector;

public class MetaWeblogImpl implements MetaWeblog
{

    private static final Logger log = LoggerFactory.getLogger(MetaWeblogImpl.class);

    private final BloggingUtils bloggingUtils;

    private final TransactionTemplate transactionTemplate;

    private final SpaceManager spaceManager;

    private final SpacePermissionManager spacePermissionManager;

    private final PageManager pageManager;

    private final PermissionManager permissionManager;

    private final LabelManager labelManager;

    private final SettingsManager settingsManager;

    // Static struc fields
    public static final String TITLE = "title";
    public static final String LINK = "link";
    public static final String PERMALINK = "permaLink";
    public static final String DESCRIPTION = "description";
    public static final String CATEGORIES = "categories";
    public static final String AUTHOR = "author";
    public static final String GUID = "guid";
    public static final String PUBDATE = "pubDate";
    public static final String DATECREATED = "dateCreated";
    public static final String HTMLURL = "htmlUrl";
    public static final String RSSURL = "rssUrl";
    public static final String POSTID = "postid";
    public static final String BLOGID = "blogid";

    public MetaWeblogImpl(BloggingUtils bloggingUtils, TransactionTemplate transactionTemplate, SpaceManager spaceManager, SpacePermissionManager spacePermissionManager, PageManager pageManager, PermissionManager permissionManager, LabelManager labelManager, SettingsManager settingsManager)
    {
        this.bloggingUtils = bloggingUtils;
        this.transactionTemplate = transactionTemplate;
        this.spaceManager = spaceManager;
        this.spacePermissionManager = spacePermissionManager;
        this.pageManager = pageManager;
        this.permissionManager = permissionManager;
        this.labelManager = labelManager;
        this.settingsManager = settingsManager;
    }

    /**
     * <p>
     * Implementation of the metaWeblog newPost method. Refer to the <a
     * href="http://www.scripting.com/images/leftArrow.gif">metaweblog
     * documentation</a> for more information.
     * </p>
     *
     * @param blogid   the Confluence space the blog post is to be created in
     * @param username
     * @param password
     * @param struct   The struct containing the blog post
     * @param publish  this flag must be set to true. Saving draft edits is not
     *                 supported through the Blogging API.
     * @return string containing the id of the newly created blogpost
     * @throws RemoteException
     */
    public String newPost(final String blogid, final String username, final String password, final Hashtable<String, Object> struct, final boolean publish)
            throws RemoteException
    {
        // Not publishing is not an option
        if (!publish)
            throw new NotImplementedException(bloggingUtils.getText("error.draft.notimplemented"));

        Object result = transactionTemplate.execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                try
                {
                    ConfluenceUser user = bloggingUtils.authenticateUser(username, password);

                    // Try to get the space specified by blogid
                    Space space = spaceManager.getSpace(blogid);

                    if (space == null)
                        throw new RemoteException(bloggingUtils.getText("error.validation.space.unknown", new String[]{blogid}));

                    // Check that the user is actually allowed to create blogposts in this
                    // space
                    if (!spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user))
                        throw new NotPermittedException(
                                bloggingUtils.getText("error.permission.edit.space.blog", new String[]{space.getKey()}));

                    // Check that a title has been supplied
                    String title = (String) struct.get(TITLE);
                    if (StringUtils.isBlank(title))
                        throw new RemoteException(bloggingUtils.getText("error.validation.blog.title.blank"));

                    // Get the content (allow blank contents)
                    String contents = StringUtils.defaultString((String) struct.get(DESCRIPTION));

                    // Get the created Date
                    Calendar publishDate = Calendar.getInstance();
                    Date dateCreated = (Date) struct.get(DATECREATED);
                    if (dateCreated != null)
                        publishDate.setTime(dateCreated);


                    // check that the blog post doesn't already exist
                    BlogPost blogPost = pageManager.getBlogPost(space.getKey(), title, publishDate);

                    if (blogPost != null)
                        throw new RemoteException(
                                bloggingUtils.getText("error.validation.blog.duplicate", new String[]{blogPost.getTitle(), publishDate.toString()}));

                    blogPost = new BlogPost();
                    blogPost.setCreationDate(new Date(publishDate.getTimeInMillis()));
                    blogPost.setSpace(space);
                    blogPost.setTitle(title);
                    blogPost.setBodyAsString(contents);
                    blogPost.setCreator(user);

                    pageManager.saveContentEntity(blogPost, null);

                    // Add the any labels to the blog post
                    @SuppressWarnings("unchecked")
                    Vector<String> categories = (Vector<String>) struct.get(CATEGORIES);
                    if (categories != null)
                        for (String category : categories)
                        {
                            Label label = new Label(category);
                            labelManager.addLabel(blogPost, label);
                        }


                    return blogPost.getIdAsString();
                }
                catch (RemoteException re)
                {
                    return re;
                }
                finally
                {
                    AuthenticatedUserThreadLocal.reset();
                }
            }
        });

        if (result instanceof RemoteException)
            throw (RemoteException) result;
        else if (result instanceof Exception)
            throw new RemoteException((Exception) result);
        else
            return (String) result;
    }

    /**
     * <p>
     * Implementation of the metaWeblog editPost method. Refer to the <a
     * href="http://www.scripting.com/images/leftArrow.gif">metaweblog
     * documentation</a> for more information.
     * </p>
     * <p>
     * Once a blog post has been created in Confluence its title cannot be
     * changed.
     * </p>
     *
     * @param postid   The id of the blog post to be edited
     * @param username
     * @param password
     * @param struct   The struct containing the updated blog post
     * @param publish  this flag must be set to true. Saving draft edits is not
     *                 supported through the Blogging API.
     * @return boolean true if the update has been successful
     * @throws RemoteException
     */
    public boolean editPost(final String postid, final String username, final String password, final Hashtable<String, Object> struct, final boolean publish)
            throws RemoteException
    {

        if (log.isDebugEnabled())
        {
            log.debug("editPost:  postid: " + postid + " username: " + username + " struct: " + struct + " publish: "
                    + publish);
        }

        // Not publishing is not an option
        if (!publish)
        {
            throw new NotImplementedException(bloggingUtils.getText("error.draft.notimplemented"));
        }

        // Run the actual logic within a transaction
        Object result = transactionTemplate.execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                try
                {
                    User user = bloggingUtils.authenticateUser(username, password);

                    // Try to get the blog post from its ID
                    BlogPost blogPost = pageManager.getBlogPost(Long.parseLong(postid));
                    if (blogPost == null)
                        throw new RemoteException(bloggingUtils.getText("error.validation.blog.doesnotexists", new String[]{postid}));

                    BlogPost originalPost;
                    try
                    {
                        originalPost = (BlogPost) blogPost.clone();
                    }
                    catch (CloneNotSupportedException e)
                    {
                        throw new RemoteException(bloggingUtils.getText("error.unexpected.blog.couldnotclone"));
                    }


                    // Get the space from the blog post and check that the user is allowed
                    // to edit the post before editing it
                    Space space = blogPost.getSpace();
                    if (!spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user))
                        throw new NotPermittedException(bloggingUtils.getText("error.permission.edit.space.blog", new String[]{space.getKey()}));

                    Date dateCreated = null == struct.get(DATECREATED)
                            ? blogPost.getCreationDate()
                            : (Date) struct.get(DATECREATED);
                    String title = StringUtils.defaultString((String) struct.get(TITLE), blogPost.getTitle());
                    String content = StringUtils.defaultString((String) struct.get(DESCRIPTION), blogPost.getBodyAsString());


                    // check that the blog post doesn't already exist on the new
                    // date with the new title
                    if (!(blogPost.getCreationDate().equals(struct.get(DATECREATED))
                            && StringUtils.equals(title, blogPost.getTitle())
                            && StringUtils.equals(content, blogPost.getBodyAsString())))
                    {
                        if (!StringUtils.equals(title, blogPost.getTitle()))
                        {
                            Calendar publishDate = Calendar.getInstance();
                            publishDate.setTime(dateCreated);
                            BlogPost testBlogPost = pageManager.getBlogPost(space.getKey(), title, publishDate);

                            if (testBlogPost != null)
                                throw new RemoteException(bloggingUtils.getText("error.validation.blog.duplicate", new String[]{title, publishDate.toString()}));
                        }

                        blogPost.setCreationDate(dateCreated);
                        blogPost.setTitle(title);
                        blogPost.setBodyAsString(content);

                        pageManager.saveContentEntity(blogPost, originalPost, null);
                    }

                    // Update the labels on the blog post
                    labelManager.removeAllLabels(blogPost);
                    @SuppressWarnings("unchecked")
                    Vector<String> categories = (Vector<String>) struct.get(CATEGORIES);
                    if (categories != null)
                        for (String category : categories)
                            if (StringUtils.isNotBlank(category))
                            {

                                Label label = new Label(StringUtils.trim(category));
                                labelManager.addLabel(blogPost, label);
                            }

                    return true;
                }
                catch (RemoteException re)
                {
                    return re;
                }
                finally
                {
                    AuthenticatedUserThreadLocal.setUser(null);
                }
            }
        });

        if (result instanceof RemoteException)
            throw (RemoteException) result;
        else if (result instanceof Exception)
            throw new RemoteException((Exception) result);
        else
            return (Boolean) result;
    }


    /**
     * Implementation of the metaWeblog getRecentPosts method. Refer to the <a
     * href="http://www.scripting.com/images/leftArrow.gif">metaweblog
     * documentation</a> for more information.
     *
     * @param blogid        the Confluence space id to the the recent news posts from.
     * @param username
     * @param password
     * @param numberOfPosts the number of posts to be returned. If the number of posts in
     *                      the space is less then the number specified all the posts from
     *                      the space will be returned.
     * @return Vector
     * @throws RemoteException
     */
    public Vector<Hashtable<String, Object>> getRecentPosts(final String blogid, final String username, final String password, final int numberOfPosts)
            throws RemoteException
    {
        Object result = transactionTemplate.execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                try
                {
                    User user = bloggingUtils.authenticateUser(username, password);

                    // Try to get the space specified by blogid
                    Space space = spaceManager.getSpace(blogid);

                    if (space == null)
                        throw new RemoteException(bloggingUtils.getText("error.validation.space.unknown", new String[]{blogid}));

                    // Check the user has permission to view the blog posts in the specified
                    // space
                    if (!spacePermissionManager.hasPermission(SpacePermission.VIEWSPACE_PERMISSION, space, user))
                        throw new NotPermittedException(
                                bloggingUtils.getText("error.permission.view.space.blog", new String[]{blogid}));

                    @SuppressWarnings("unchecked")
                    List<BlogPost> recentPosts = pageManager.getRecentlyAddedBlogPosts(numberOfPosts, space.getKey());
                    Vector<Hashtable<String, Object>> retVector = new Vector<Hashtable<String, Object>>();

                    for (BlogPost blogPost : recentPosts)
                        retVector.add(getBlogPostAsHashtable(blogPost));

                    return retVector;
                }
                catch (RemoteException re)
                {
                    return re;
                }
                finally
                {
                    AuthenticatedUserThreadLocal.setUser(null);
                }
            }
        });

        if (result instanceof RemoteException)
            throw (RemoteException) result;
        else if (result instanceof Exception)
            throw new RemoteException((Exception) result);
        else
            return (Vector<Hashtable<String, Object>>) result;
    }


    /**
     * Implementation of the metaweblog getPost method. Refer to the <a
     * href="http://www.scripting.com/images/leftArrow.gif">metaweblog
     * documentation</a> for more information.
     *
     * @param postid   the id of the Confluence blog post to be returned
     * @param username
     * @param password
     * @throws RemoteException
     * @returns struct
     */
    public Hashtable<String, Object> getPost(final String postid, final String username, final String password) throws RemoteException
    {
        Object result = transactionTemplate.execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                try
                {
                    User user = bloggingUtils.authenticateUser(username, password);

                    // Check if the blog post exists
                    BlogPost blogPost;
                    try
                    {
                        blogPost = pageManager.getBlogPost(Long.parseLong(postid));

                        if (blogPost == null)
                            throw new RemoteException(bloggingUtils.getText("error.validation.blog.doesnotexists", new String[]{postid}));
                    }
                    catch (NumberFormatException nfe)
                    {
                        throw new RemoteException(
                                bloggingUtils.getText(
                                        "error.validation.blog.invalidpostid"));
                    }

                    // Check that the user is allowed to view the blog post.
                    if (!permissionManager.hasPermission(user, Permission.VIEW, blogPost))
                        throw new NotPermittedException(bloggingUtils.getText("error.permission.view.blog", new String[]{postid}));

                    return getBlogPostAsHashtable(blogPost);
                }
                catch (RemoteException re)
                {
                    return re;
                }
                finally
                {
                    AuthenticatedUserThreadLocal.setUser(null);
                }
            }
        });

        if (result instanceof RemoteException)
            throw (RemoteException) result;
        else if (result instanceof Exception)
            throw new RemoteException((Exception) result);
        else
            return (Hashtable<String, Object>) result;

    }

    /**
     * Builds a blog struct for returning to metaWeblog calls from a given
     * blogPost. The following fields are put into the struct:
     *
     * @param blogPost
     * @return struct representing the given blog post
     */
    protected Hashtable<String, Object> getBlogPostAsHashtable(BlogPost blogPost)
    {

        Hashtable<String, Object> blogStruct = new Hashtable<String, Object>();

        blogStruct.put(POSTID, blogPost.getIdAsString());
        blogStruct.put(BLOGID, blogPost.getSpace().getKey());
        blogStruct.put(TITLE, StringUtils.defaultString(blogPost.getTitle(), ""));
        blogStruct.put(DESCRIPTION, bloggingUtils.convertStorageFormatToView(blogPost));
        blogStruct.put(PUBDATE, blogPost.getCreationDate());
        blogStruct.put(DATECREATED, blogPost.getCreationDate());
        blogStruct.put(AUTHOR, StringUtils.defaultString(blogPost.getCreatorName(), UserAccessor.ANONYMOUS));

        String link = new StringBuilder(settingsManager.getGlobalSettings().getBaseUrl()).append("/").append(blogPost.getUrlPath()).toString();
        blogStruct.put(LINK, link);
        blogStruct.put(PERMALINK, link);

        // Filter out the labels the user shouldn't see
        @SuppressWarnings("unchecked")
        List<Label> visibleLabels = LabelPermissionSupport.filterVisibleLabels(blogPost.getLabels(), AuthenticatedUserThreadLocal.getUser(), true);
        Vector<String> categories = new Vector<String>();
        for (Label visibleLabel : visibleLabels)
            categories.add(visibleLabel.toString());
        blogStruct.put(CATEGORIES, categories);

        return blogStruct;
    }

    /**
     * Implementation of the MetaWeblog getCategories method. This method
     * returns a struct containing confluence all of the labels from the space
     * represented by the blogid as well as 20 of the most popular global
     * labels.
     * <p/>
     * Refer to to the <a
     * href="http://www.xmlrpc.com/metaWeblogApi#metawebloggetcategories">metaweblog
     * documentation</a> for more information.
     *
     * @param blogid   The confluence space id to get labels for
     * @param username
     * @param password
     * @return struct
     * @throw RemoteException
     */
    public Hashtable<String, Hashtable<String, String>> getCategories(final String blogid, final String username, final String password) throws RemoteException
    {
        Object result = transactionTemplate.execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                try
                {
                    User user = bloggingUtils.authenticateUser(username, password);

                    // Try to get the space specified by blogid
                    Space space = spaceManager.getSpace(blogid);

                    if (space == null)
                        throw new RemoteException(bloggingUtils.getText("error.validation.space.unknown", new String[]{blogid}));

                    // Check the user has permission to view the blog posts in the specified
                    // space
                    if (!spacePermissionManager.hasPermission(SpacePermission.VIEWSPACE_PERMISSION, space, user))
                        throw new NotPermittedException(bloggingUtils.getText("error.permission.view.space.blog", new String[]{blogid}));

                    // The 'Charles' label return method:
                    // the union of the 20 most popular global labels, and all labels from
                    // the space being posted to
                    @SuppressWarnings("unchecked")
                    List<Label> labelsInSpace = labelManager.getLabelsInSpace(space.getKey());
                    @SuppressWarnings("unchecked")
                    List<LabelSearchResult> popularLabelsSearchResult = labelManager.getMostPopularLabels(20);
                    Set<Label> labels = new HashSet<Label>();

                    labels.addAll(labelsInSpace);

                    // Popular labels are returned as LabelSearchResult objects
                    for (LabelSearchResult popularLabelResult : popularLabelsSearchResult)
                        labels.add(popularLabelResult.getLabel());

                    // Filter out labels the current user isn't supposed to see.
                    // (the current methods used here don't seem to return
                    // personal labels any way, but it isn't really defined in
                    // the javadocs what is returned so this check is here just
                    // in case.)
                    @SuppressWarnings("unchecked")
                    List<Label> visibleLabels = LabelPermissionSupport.filterVisibleLabels(new ArrayList<Label>(labels), AuthenticatedUserThreadLocal.getUser(), true);

                    Hashtable<String, Hashtable<String, String>> returnStruct = new Hashtable<String, Hashtable<String, String>>();
                    String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
                    for (Label visibleLabel : visibleLabels)
                    {
                        Hashtable<String, String> struct = new Hashtable<String, String>();

                        struct.put(DESCRIPTION, visibleLabel.toString());
                        struct.put(HTMLURL, new StringBuilder(baseUrl).append(visibleLabel.getUrlPath()).toString());
                        struct.put(RSSURL, new StringBuilder(baseUrl)
                                .append("/createrssfeed.action?types=page&types=blogpost&types=mail&types=comment&types=attachment&statuses=created&statuses=modified&showContent=true&showDiff=true")
                                .append("&spaces=" + space.getKey())
                                .append("&labelString=" + visibleLabel.toString())
                                .append("&rssType=atom&maxResults=20&timeSpan=30&publicFeed=false&title=Confluence+Label+RSS+Feed&os_authType=basic")
                                .toString());

                        returnStruct.put(visibleLabel.toString(), struct);
                    }

                    return returnStruct;
                }
                catch (RemoteException re)
                {
                    return re;
                }
                finally
                {
                    AuthenticatedUserThreadLocal.setUser(null);
                }
            }
        });

        if (result instanceof RemoteException)
            throw (RemoteException) result;
        else if (result instanceof Exception)
            throw new RemoteException((Exception) result);
        else
            return (Hashtable<String, Hashtable<String, String>>) result;
    }

    public Hashtable<String, Object> newMediaObject(String blogid, String username, String password, Hashtable struct)
            throws RemoteException
    {
        throw new NotImplementedException(bloggingUtils.getText("error.newmediaobject.notimplemented"));
    }
}
