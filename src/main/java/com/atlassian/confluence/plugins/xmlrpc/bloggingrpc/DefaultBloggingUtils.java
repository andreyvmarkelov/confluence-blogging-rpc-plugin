package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.rpc.AuthenticationFailedException;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DefaultBloggingUtils implements BloggingUtils
{
    private static final Logger LOG = LoggerFactory.getLogger(DefaultBloggingUtils.class);

    private final UserAccessor userAccessor;

    private final SpaceManager spaceManager;

    private final SpacePermissionManager spacePermissionManager;

    private final LocaleManager localeManager;

    private final I18NBeanFactory i18NBeanFactory;

    private final XhtmlContent xhtmlContent;

    public DefaultBloggingUtils(UserAccessor userAccessor, SpaceManager spaceManager, SpacePermissionManager spacePermissionManager, LocaleManager localeManager, I18NBeanFactory i18NBeanFactory, XhtmlContent xhtmlContent)
    {
        this.userAccessor = userAccessor;
        this.spaceManager = spaceManager;
        this.spacePermissionManager = spacePermissionManager;
        this.localeManager = localeManager;
        this.i18NBeanFactory = i18NBeanFactory;
        this.xhtmlContent = xhtmlContent;
    }

    /*    public static final String AUTHENTICATION_FAILED_MSG =	"Invalid User Name or Password";
public static final String ANONYMOUS_USER = "Anonymous";
public static final String DRAFT_NOT_IMPLEMENTED_MSG = "You cannot save a draft version through the Confluence Blogging API. You must publish your updates immediately.";*/

    /**
     * Authenticate that a given user exists with the given password. If the
     * user doesn't exist or the password is incorrect this method will throw an
     * authentication exception. If the user does exist the the password is
     * correct the corresponding User object will be returned. If the user is
     * authenticated successfully the user object is also set into the
     * AuthenticatetUserThreadLocal object so any actions performed will appear
     * to be done by the authenticated user.
     * <p/>
     * This method does not create a transaction so calling code must wrap this
     * method in a transaction.
     *
     * @param username
     * @param password
     * @return User object corresponding to the given username and password.
     * @throws com.atlassian.confluence.rpc.AuthenticationFailedException
     *          if the username doesn't exist or the password is incorrect an
     *          AuthenticationFailedException will be thrown.
     */
    @Override
    public ConfluenceUser authenticateUser(String username, String password) throws AuthenticationFailedException
    {
        ConfluenceUser user = userAccessor.getUserByName(username);

        if (user == null)
        {
            throw new AuthenticationFailedException(getText("error.validation.authentication"));
        }
        boolean authenticated = userAccessor.authenticate(username, password);

        if (authenticated)
        {
            AuthenticatedUserThreadLocal.set(user);
            return user;
        }
        else
        {
            throw new AuthenticationFailedException(getText("error.validation.authentication"));
        }
    }


    /**
     * Returns a list of Space objects that the given user is allowed to create
     * blog posts in
     *
     * @param user the user to get the allowed spaces for
     * @return List of spaces that the given user can create blog posts in. List
     *         is sorted by space name.
     */
    @Override
    public List<Space> getBlogs(User user)
    {
        List<Space> blogs = new ArrayList<Space>();

        if (user != null)
        {
            List<Space> allSpaces = spaceManager.getAllSpaces();
            for (Space space: allSpaces)
            {
                if (spacePermissionManager.hasPermission(
                        SpacePermission.EDITBLOG_PERMISSION, space, user))
                {
                    blogs.add(space);
                }

            }

            Collections.sort(blogs, new Comparator<Space>()
            {
                public int compare(Space left, Space right)
                {
                    return left.getName().compareToIgnoreCase(right.getName());
                }
            }
            );
        }

        return blogs;
    }

    @Override
    public String getText(String s)
    {
        return getI18nBean().getText(s);
    }

    private I18NBean getI18nBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }

    @Override
    public String getText(String s, String s1)
    {
        return this.getText(s, Arrays.asList(s1));
    }

    public String getText(String s, Object[] objects)
    {
        return getI18nBean().getText(s, objects);
    }

    public String getText(String s, List<?> list)
    {
        return getI18nBean().getText(s, list);
    }

    @Override
    public String convertStorageFormatToView(BlogPost blogPost)
    {
        try
        {
            return xhtmlContent.convertStorageToView(
                    blogPost.getBodyAsString(),
                    new DefaultConversionContext(blogPost.toPageContext())
            );
        }
        catch (XhtmlException xhtmlError)
        {
            LOG.error(String.format("Unable to convert content of %s to view. There's a problem with the markup", blogPost.getIdAsString()), xhtmlError);
        }
        catch (XMLStreamException xmlError)
        {
            LOG.error(String.format("Unable to convert content of %s to view. Unable to stream storage markup", blogPost.getIdAsString()), xmlError);
        }

        return blogPost.getBodyAsString();
    }
}
