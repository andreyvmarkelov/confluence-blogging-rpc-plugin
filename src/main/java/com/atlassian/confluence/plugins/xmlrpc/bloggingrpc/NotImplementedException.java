package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.rpc.RemoteException;

/**
 * Simple Exception for functionality that has not yet been implemented, or will
 * never be implemented.
 *
 * @author Shannon Krebs
 */
public class NotImplementedException extends RemoteException
{

    private static final long serialVersionUID = 3180260694128747674L;

    public NotImplementedException()
    {
        super();
    }

    public NotImplementedException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);
    }

    public NotImplementedException(String arg0)
    {
        super(arg0);
    }

    public NotImplementedException(Throwable arg0)
    {
        super(arg0);
    }

}
