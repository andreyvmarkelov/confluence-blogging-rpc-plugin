package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.AuthenticationFailedException;
import com.atlassian.confluence.rpc.NotPermittedException;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of the Blogger Interface
 *
 * @author Shannon Krebs
 */
public class BloggerImpl implements Blogger
{

    private static Logger log = LoggerFactory.getLogger(BloggerImpl.class);

    private final TransactionTemplate transactionTemplate;

    private final BloggingUtils bloggingUtils;

    private final SpaceManager spaceManager;

    private final SpacePermissionManager spacePermissionManager;

    private final PageManager pageManager;

    private final PermissionManager permissionManager;

    private final SettingsManager settingsManager;

    public static final String TITLE = "title";
    public static final String CONTENT = "content";
    public static final String POSTID = "postid";
    public static final String BLOGID = "blogid";
    public static final String BLOG_NAME = "blogName";
    public static final String URL = "url";
    public static final String DATE_CREATED = "dateCreated";
    public static final String AUTHOR_NAME = "authorName";
    public static final String AUTHOR_EMAIL = "authorEmail";
    public static final String FIRST_NAME = "firstname";
    public static final String LAST_NAME = "lastname";
    public static final String EMAIL = "email";
    public static final String USER_ID = "userid";

    public BloggerImpl(BloggingUtils bloggingUtils, TransactionTemplate transactionTemplate, SpaceManager spaceManager, SpacePermissionManager spacePermissionManager, PageManager pageManager, PermissionManager permissionManager, SettingsManager settingsManager)
    {
        this.bloggingUtils = bloggingUtils;
        this.transactionTemplate = transactionTemplate;
        this.spaceManager = spaceManager;
        this.spacePermissionManager = spacePermissionManager;
        this.pageManager = pageManager;
        this.permissionManager = permissionManager;
        this.settingsManager = settingsManager;
    }

    /**
     * Implementation of the Blogger getUserBlogs API Method.
     *
     * @param appKey   required as part of the blogger API, not used in this
     *                 Implementation
     * @param username the username to get the blogs for
     * @param password the password for the user
     * @return vector of structs containing the authenticated user's blogs
     * @throws AuthenticationFailedException if the username is not found or the password is incorrect.
     */
    public Vector<Hashtable<String, String>> getUsersBlogs(final String appKey, final String username, final String password) throws RemoteException
    {
        Object result = transactionTemplate.execute(new TransactionCallback<Object>()
        {

            @Override
            public Object doInTransaction()
            {
                try
                {
                    Vector<Hashtable<String, String>> blogs = new Vector<Hashtable<String, String>>();

                    for (Space space : bloggingUtils.getBlogs(bloggingUtils.authenticateUser(username, password)))
                    {
                        // Build the blog struct to add to the list
                        Hashtable<String, String> blog = new Hashtable<String, String>();

                        blog.put(BLOG_NAME, space.getName());
                        blog.put(URL, new StringBuilder(settingsManager.getGlobalSettings().getBaseUrl())
                                .append("/pages/viewrecentblogposts.action?key=").append(space.getKey())
                                .toString());
                        blog.put(BLOGID, space.getKey());
                        blogs.add(blog);
                    }

                    return blogs;
                }
                catch (AuthenticationFailedException auth)
                {
                    return auth;
                }
                finally
                {
                    AuthenticatedUserThreadLocal.setUser(null);
                }
            }
        });

        if (result instanceof RemoteException)
            throw (RemoteException) result;
        else if (result instanceof Exception)
            throw new RemoteException((Exception) result);
        else
            return (Vector<Hashtable<String, String>>) result;
    }

    public String newPost(final String appKey, final String blogid, final String username, final String password, final String content, final boolean publish) throws RemoteException
    {
        // Not publishing is not an option
        if (!publish)
            throw new NotImplementedException(bloggingUtils.getText("error.draft.notimplemented"));

        Object result = transactionTemplate.execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                try
                {
                    // Authenticate the user
                    ConfluenceUser user = bloggingUtils.authenticateUser(username, password);

                    // Try to get the space specified by blogid
                    Space space = spaceManager.getSpace(blogid);

                    if (space == null)
                        throw new RemoteException(bloggingUtils.getText("error.validation.space.unknown", new String[]{blogid}));

                    // Check that the user is actually allowed to create blogposts in this
                    // space
                    if (!spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user))
                        throw new NotPermittedException(bloggingUtils.getText("error.permission.edit.space.blog", new String[]{space.getKey()}));

                    Map<String, String> blogMap = splitBlogContent(content);

                    // Check that the title has been specified
                    String title = blogMap.get(TITLE);
                    if (StringUtils.isBlank(title))
                        throw new RemoteException(bloggingUtils.getText("error.validation.blog.title.blank"));

                    String cleanContent = blogMap.get(CONTENT);

                    Calendar publishDate = Calendar.getInstance();


                    // check that the blog post doesn't already exist
                    BlogPost blogPost = pageManager.getBlogPost(space.getKey(), title, publishDate);

                    if (blogPost != null)
                        throw new RemoteException(bloggingUtils.getText("error.validation.blog.duplicate", new String[]{title, publishDate.toString()}));

                    blogPost = new BlogPost();
                    blogPost.setCreationDate(new Date(publishDate.getTimeInMillis()));
                    blogPost.setSpace(space);
                    blogPost.setBodyAsString(cleanContent);
                    blogPost.setTitle(title);
                    blogPost.setCreator(user);

                    pageManager.saveContentEntity(blogPost, null);
                    return blogPost.getIdAsString();
                }
                catch (RemoteException re)
                {
                    return re;
                }
                finally
                {
                    AuthenticatedUserThreadLocal.reset();
                }
            }
        });

        if (result instanceof RemoteException)
            throw (RemoteException) result;
        else if (result instanceof Exception)
            throw new RemoteException((Exception) result);
        else
            return (String) result;
    }

    public boolean editPost(final String appKey, final String postid, final String username, final String password, final String content, final boolean publish) throws RemoteException
    {
        if (!publish)
            throw new NotImplementedException(bloggingUtils.getText("error.draft.notimplemented"));

        Object result = transactionTemplate.execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                try
                {
                    // Authenticate the user
                    User user = bloggingUtils.authenticateUser(username, password);


                    //  Try to get the blog post from its ID
                    BlogPost blogPost = pageManager.getBlogPost(new Long(postid).longValue());


                    if (blogPost == null)
                        throw new RemoteException(bloggingUtils.getText("error.validation.blog.doesnotexists", new String[]{postid}));

                    BlogPost originalPost;
                    try
                    {
                        originalPost = (BlogPost) blogPost.clone();
                    }
                    catch (CloneNotSupportedException e)
                    {
                        throw new RemoteException(bloggingUtils.getText("error.unexpected.blog.couldnotclone"));
                    }

                    // Get the space from the blog post and check that the user is allowed
                    // to edit the post before editing it
                    Space space = blogPost.getSpace();

                    if (!spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user))
                        throw new NotPermittedException(bloggingUtils.getText("error.permission.edit.space.blog", new String[]{space.getKey()}));


                    Map blogMap = splitBlogContent(content);

                    // Check that the title has been specified
                    String title = (String) blogMap.get(TITLE);
                    if (StringUtils.isBlank(title))
                        throw new RemoteException(bloggingUtils.getText("error.validation.blog.title.blank"));

                    if (!StringUtils.equals(title, blogPost.getTitle()))
                    {
                        Calendar publishedDate = Calendar.getInstance();

                        publishedDate.setTime(blogPost.getCreationDate());

                        if (null != pageManager.getBlogPost(space.getKey(), title, publishedDate))
                            throw new RemoteException(bloggingUtils.getText("error.validation.blog.duplicate", new Object[]{title, publishedDate}));
                    }

                    blogPost.setTitle((String) blogMap.get(TITLE));
                    blogPost.setBodyAsString((String) blogMap.get(CONTENT));

                    pageManager.saveContentEntity(blogPost, originalPost, null);
                    return true;

                }
                catch (RemoteException re)
                {
                    return re;
                }
                finally
                {
                    AuthenticatedUserThreadLocal.setUser(null);
                }
            }
        });

        if (result instanceof RemoteException)
            throw (RemoteException) result;
        else if (result instanceof Exception)
            throw new RemoteException((Exception) result);
        else
            return (Boolean) result;
    }

    public Vector<Hashtable<String, Object>> getRecentPosts(final String appkey, final String blogid, final String username,
                                                            final String password, final int numposts) throws RemoteException
    {
        Object result = transactionTemplate.execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                Object returnObject;

                try
                {
                    // Authenticate the user
                    User user = bloggingUtils.authenticateUser(username, password);

                    // Try to get the space specified by blogid
                    Space space = spaceManager.getSpace(blogid);

                    if (space == null)
                        throw new RemoteException(bloggingUtils.getText("error.validation.space.unknown", new String[]{blogid}));

                    // Check the user has permission to view the blog posts in the specified
                    // space
                    if (!spacePermissionManager.hasPermission(SpacePermission.VIEWSPACE_PERMISSION, space, user))
                        throw new NotPermittedException(
                                bloggingUtils.getText("error.permission.view.space.blog", new String[]{space.getKey()}));


                    @SuppressWarnings("unchecked")
                    List<BlogPost> recentPosts = pageManager.getRecentlyAddedBlogPosts(numposts, space.getKey());
                    Vector<Hashtable<String, Object>> recentPostsVector = new Vector<Hashtable<String, Object>>();

                    if (log.isDebugEnabled())
                        log.debug(String.format("Found %d blogs in space: %s", recentPosts.size(), space.getKey()));

                    // for each recently added blog
                    for (BlogPost blogPost : recentPosts)
                        recentPostsVector.add(getBlogPostAsHashtable(space, blogPost, user));

                    return recentPostsVector;
                }
                catch (RemoteException re)
                {
                    returnObject = re;
                }
                finally
                {
                    AuthenticatedUserThreadLocal.setUser(null);
                }

                return returnObject;
            }
        });

        if (result instanceof RemoteException)
            throw (RemoteException) result;
        else if (result instanceof Exception)
            throw new RemoteException((Exception) result);
        else
            return (Vector<Hashtable<String, Object>>) result;
    }

    private Hashtable<String, Object> getBlogPostAsHashtable(Space space, BlogPost blogPost, User forUser)
    {
        String title = StringUtils.defaultString(blogPost.getTitle());

        // Convert the blogpost into a struct to be returned
        Hashtable<String, Object> blogStruct = new Hashtable<String, Object>();
        blogStruct.put(POSTID, blogPost.getIdAsString());
        blogStruct.put(BLOGID, space.getKey());
        blogStruct.put(TITLE, title);
        blogStruct.put(URL, settingsManager.getGlobalSettings().getBaseUrl() + blogPost.getUrlPath());
        blogStruct.put(CONTENT, new StringBuilder("<title>").append(title).append("</title>").append(bloggingUtils.convertStorageFormatToView(blogPost)).toString());
        blogStruct.put(DATE_CREATED, blogPost.getCreationDate());
        blogStruct.put(AUTHOR_NAME, StringUtils.defaultIfEmpty(blogPost.getCreatorName(), UserAccessor.ANONYMOUS));
        blogStruct.put(AUTHOR_EMAIL, StringUtils.defaultString(forUser.getEmail()));

        return blogStruct;
    }

    public boolean deletePost(final String appkey, final String postid, final String username, final String password, final boolean publish) throws RemoteException
    {
        // Not publishing is not an option
        if (!publish)
            throw new NotImplementedException(bloggingUtils.getText("error.draft.notimplemented"));

        Object result = transactionTemplate.execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                try
                {
                    // Authenticate the user
                    User user = bloggingUtils.authenticateUser(username, password);

                    // Try to get the blog post from its ID
                    BlogPost blogPost = pageManager.getBlogPost(new Long(postid).longValue());

                    if (blogPost == null)
                        throw new RemoteException(bloggingUtils.getText("error.validation.blog.doesnotexists", new String[]{postid}));

                    // Get the space from the blog post and check that the user is allowed
                    // to delete the post before deleting it
                    Space space = blogPost.getSpace();

                    if (spacePermissionManager.hasPermission(SpacePermission.REMOVE_BLOG_PERMISSION, space, user))
                    {
                        blogPost.trash();
                        return true;
                    }
                    else
                    {
                        throw new NotPermittedException(bloggingUtils.getText("error.permission.remove.space.blog", new String[]{space.getKey()}));
                    }
                }
                catch (RemoteException re)
                {
                    return re;
                }
                finally
                {
                    AuthenticatedUserThreadLocal.setUser(null);
                }
            }
        });

        if (result instanceof RemoteException)
            throw (RemoteException) result;
        else if (result instanceof Exception)
            throw new RemoteException((Exception) result);
        else
            return (Boolean) result;
    }

    public Hashtable<String, Object> getPost(final String appkey, final String postid, final String username, final String password) throws RemoteException
    {
        Object result = transactionTemplate.execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                try
                {
                    // Authenticate the user
                    User user = bloggingUtils.authenticateUser(username, password);

                    BlogPost blogPost;
                    try
                    {
                        long postIdLong = Long.parseLong(postid);

                        blogPost = pageManager.getBlogPost(postIdLong);
                        if (null == blogPost)
                            throw new RemoteException(bloggingUtils.getText("error.validation.blog.doesnotexists", new String[]{postid}));
                    }
                    catch (NumberFormatException nan)
                    {
                        throw new RemoteException(bloggingUtils.getText("error.validation.blog.invalidpostid"));
                    }

                    // Check that the user is allowed to view the blog post.
                    if (!permissionManager.hasPermission(user, Permission.VIEW, blogPost))
                        throw new RemoteException(bloggingUtils.getText("error.permission.view.blog", new String[]{postid}));

                    return getBlogPostAsHashtable(blogPost.getSpace(), blogPost, user);
                }
                catch (RemoteException re)
                {
                    return re;
                }
                finally
                {
                    AuthenticatedUserThreadLocal.setUser(null);
                }
            }
        });

        if (result instanceof RemoteException)
            throw (RemoteException) result;
        else if (result instanceof Exception)
            throw new RemoteException((Exception) result);
        else
            return (Hashtable<String, Object>) result;
    }

    public Hashtable<String, String> getUserInfo(final String appkey, final String username, final String password)
            throws RemoteException
    {
        Object result = transactionTemplate.execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                try
                {

                    // Authenticate the user
                    User user = bloggingUtils.authenticateUser(username, password);

                    Hashtable<String, String> userInformation = new Hashtable<String, String>();

                    userInformation.put(FIRST_NAME, user.getFullName());
                    userInformation.put(EMAIL, user.getEmail());
                    userInformation.put(USER_ID, user.getName());

                    return userInformation;
                }
                catch (RemoteException re)
                {
                    return re;
                }
                finally
                {
                    AuthenticatedUserThreadLocal.setUser(null);
                }
            }
        });

        if (result instanceof RemoteException)
            throw (RemoteException) result;
        else if (result instanceof Exception)
            throw new RemoteException((Exception) result);
        else
            return (Hashtable<String, String>) result;
    }

    public String getTemplate(String appkey, String blogid, String username,
                              String password, String templateType) throws RemoteException
    {
        throw new NotImplementedException(bloggingUtils.getText("error.template.notimplemented"));
    }

    public boolean setTemplate(String appkey, String blogid, String username,
                               String password, String template, String templateType) throws RemoteException
    {
        throw new NotImplementedException(bloggingUtils.getText("error.template.notimplemented"));
    }

    /**
     * <p>
     * Utility method to split out the title of the blog post from the content
     * input string. This method will return a map containing the title and the
     * contents without the title.
     * </p>
     * <p>
     * The title is represented by
     * <p/>
     * <pre>
     * &lt;title&gt;&lt;/title&gt;
     * </pre>
     * <p/>
     * tags at the start of the content.
     * <p>
     * If the title is not found in the content all of the content will be
     * returned mapped to the CONTENT key.
     * </p>
     *
     * @param originalContent containing the title
     * @return map containing the title and the contents without the title. Keys
     *         are static varibles declared in this class.
     */
    Map<String, String> splitBlogContent(String originalContent)
    {

        Map<String, String> splitContent = new HashMap<String, String>();

        Pattern pattern = Pattern.compile("<[tT][iI][tT][lL][eE]>.*?</[tT][iI][tT][lL][eE]>");
        Matcher matcher = pattern.matcher(originalContent);

        if (matcher.find())
        {
            String fullTitleString = matcher.group();
            String title = fullTitleString.substring(7, (fullTitleString.length() - 8));
            String contents = originalContent.substring(matcher.end());

            splitContent.put(TITLE, title);
            splitContent.put(CONTENT, contents);

        }
        else
        {
            splitContent.put(CONTENT, originalContent);
        }


        return splitContent;
    }
}
