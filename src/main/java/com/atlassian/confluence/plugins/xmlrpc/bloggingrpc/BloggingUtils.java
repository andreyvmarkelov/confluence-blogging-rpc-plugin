package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.rpc.AuthenticationFailedException;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.user.User;

import java.util.List;

/**
 * Utility class to handle common functionality from the blogging API implementations
 *
 * @author Shannon Krebs
 */
public interface BloggingUtils
{
    ConfluenceUser authenticateUser(String username, String password) throws AuthenticationFailedException;

    List<Space> getBlogs(User user);

    String getText(String s);

    String getText(String s, String s1);

    String getText(String s, Object[] s1);

    String convertStorageFormatToView(BlogPost blogPost);
}
