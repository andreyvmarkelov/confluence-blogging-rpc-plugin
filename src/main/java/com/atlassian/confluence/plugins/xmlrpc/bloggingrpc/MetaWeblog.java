package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.rpc.RemoteException;

import java.util.Hashtable;
import java.util.Vector;

/**
 * Definition of the MeatWeblog API as an Interface. Refer to the <a
 * href="http://www.xmlrpc.com/metaWeblogApi">MetaWeblog API</a> for a
 * description of each methods functionality.
 *
 * @author Shannon Krebs
 */
public interface MetaWeblog
{

    public String newPost(String blogid, String username, String password, Hashtable<String, Object> struct, boolean publish)
            throws RemoteException;

    public boolean editPost(String postid, String username, String password, Hashtable<String, Object> struct, boolean publish)
            throws RemoteException;

    public Hashtable<String, Object> getPost(String postid, String username, String password) throws RemoteException;

    public Hashtable<String, Hashtable<String, String>> getCategories(String blogid, String username, String password) throws RemoteException;

    public Vector<Hashtable<String, Object>> getRecentPosts(String blogid, String username, String password, int numberOfPosts)
            throws RemoteException;

    public Hashtable<String, Object> newMediaObject(String blogid, String username, String password, Hashtable struct)
            throws RemoteException;

}
