package it.com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import org.apache.xmlrpc.XmlRpcClient;

import java.util.Vector;
import java.util.Arrays;

public class AbstractBloggingRpcTestCase extends AbstractConfluencePluginWebTestCase {

    protected XmlRpcClient xmlRpcClient;

    protected void setUp() throws Exception {
        super.setUp();
        xmlRpcClient = new XmlRpcClient(getConfluenceWebTester().getBaseUrl() + "/rpc/xmlrpc");
    }

    protected Vector toXmlRpcParams(final Object[] params) {
        final Vector vector = new Vector();

        if (null != params)
            vector.addAll(Arrays.asList(params));

        return vector;
    }
}
