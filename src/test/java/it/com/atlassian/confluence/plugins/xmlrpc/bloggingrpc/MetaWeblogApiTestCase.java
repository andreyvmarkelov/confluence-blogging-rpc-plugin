package it.com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.xmlrpc.XmlRpcException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MetaWeblogApiTestCase extends AbstractBloggingRpcTestCase {

    private Object createNewBlog(final String spaceKey, final String title, final String content, final String[] labels, final boolean publish) throws XmlRpcException, IOException {
        final Hashtable blogStructure = toBlogStructure(title, content, labels);

        return xmlRpcClient.execute(
                "metaWeblog.newPost",
                toXmlRpcParams(
                        new Object[] {
                                spaceKey,
                                getConfluenceWebTester().getCurrentUserName(),
                                getConfluenceWebTester().getCurrentPassword(),
                                blogStructure,
                                publish ? Boolean.TRUE : Boolean.FALSE
                        }));
    }

    private Object updateBlog(final long blogPostId, final String title, final String content, final String[] labels, final boolean publish) throws XmlRpcException, IOException {
        final Hashtable blogStructure = toBlogStructure(title, content, labels);


        return xmlRpcClient.execute(
                "metaWeblog.editPost",
                toXmlRpcParams(
                        new Object[] {
                                String.valueOf(blogPostId),
                                getConfluenceWebTester().getCurrentUserName(),
                                getConfluenceWebTester().getCurrentPassword(),
                                blogStructure,
                                publish ? Boolean.TRUE : Boolean.FALSE
                        }));
    }

    private Hashtable toBlogStructure(String title, String content, String[] labels) {
        final Hashtable blogStructure = new Hashtable();

        blogStructure.put("title", title);
        blogStructure.put("description", content);

        if (null != labels)
            blogStructure.put("categories", new Vector(Arrays.asList(labels)));

        return blogStructure;
    }

    public void testCreateDraftBlog() throws IOException  {
        try {
            createNewBlog("ds", "Test MetaWeblog Blog", "Test Blog Content.", null, false);
            fail("XmlRpcException should be thrown because draft blogs are not supported.");
        } catch (final XmlRpcException xmlRpcE) {
            /* Success */
        }
    }

    public void testUpdateDraftBlog() throws IOException  {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test MetaWeblog Blog");
        blogPostHelper.setContent("Test Blog Content");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());

        try {
            updateBlog(blogPostHelper.getId(), "Test MetaWeblog Blog", "Edited Test Blog Content", null, false);
            fail("XmlRpcException should be thrown because draft blogs are not supported.");
        } catch (final XmlRpcException xmlRpcE) {
            /* Success */
        }

        assertTrue(blogPostHelper.delete());
    }

    public void testCreateBlog() throws XmlRpcException, IOException {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        final Object blogId;
        final BlogPostHelper blogPostHelper;

        blogId = createNewBlog("ds", "Test MetaWeblog Blog", "Test Blog Content", null, true);

        gotoPage("/display/ds/" + simpleDateFormat.format(new Date()) + "/Test+MetaWeblog+Blog");
        assertTextPresent("Test Blog Content");

        blogPostHelper = getBlogPostHelper(Long.parseLong(blogId.toString()));
        assertTrue(blogPostHelper.delete());
    }

    public void testCreateBlogWithoutTitle() throws XmlRpcException, IOException {
        try {
            createNewBlog("ds", StringUtils.EMPTY, "Test Blog Content", null, true);
            fail("XmlRpcException expected to be thrown because blog has no title (<title/>)");
        } catch (final XmlRpcException xmlRpcE) {
            /* Success */
        }
    }

    public void testCreateBlogWithDuplicateTitleInTheSamePublishedDay() throws XmlRpcException, IOException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test MetaWeblog Blog");
        blogPostHelper.setContent("Test Blog Content");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());

        try {
            createNewBlog("ds", "Test MetaWeblog Blog", "Test Blog Content", null, true);
            fail("XmlRpcException expected to be thrown because there is a blog with the same title on the same day");
        } catch (final XmlRpcException xmlRpcE) {
            /* Success */
        }

        assertTrue(blogPostHelper.delete());
    }

    public void testEditBlog() throws XmlRpcException, IOException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        final Object updateSuccessful;

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test MetaWeblog Blog");
        blogPostHelper.setContent("Test Blog Content");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());

        updateSuccessful = updateBlog(blogPostHelper.getId(), "Test MetaWeblog Blog", "Edited Test Blog Content", null, true);
        assertEquals(Boolean.TRUE, updateSuccessful);

        gotoPage("/display/ds/" + simpleDateFormat.format(new Date()) + "/Test+MetaWeblog+Blog");
        assertTextPresent("Edited Test Blog Content");

        assertTrue(blogPostHelper.delete());
    }

    public void testEditBlogWithoutTitle() throws XmlRpcException, IOException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test MetaWeblog Blog");
        blogPostHelper.setContent("Test Blog Content");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());


        updateBlog(blogPostHelper.getId(), StringUtils.EMPTY, "Edited Test Blog Content", null, true);
        /* Unlike the Blogger API, blog edits with blank title will not change the original blog title */
        gotoPage("/display/ds/" + simpleDateFormat.format(blogPostHelper.getCreationDate()) + "Test+MetaWeblog+Blog");
        assertTextPresent("Test Blog Content");

        assertTrue(blogPostHelper.delete());
    }

    public void testEditBlogWithDuplicateTitleInTheSamePublishedDay() throws XmlRpcException, IOException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final BlogPostHelper anotherBlogPostHelper = getBlogPostHelper();

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test MetaWeblog Blog");
        blogPostHelper.setContent("Test Blog Content");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());

        /* Create another blog, the one to be edited */
        anotherBlogPostHelper.setSpaceKey("ds");
        anotherBlogPostHelper.setTitle("Another MetaWeblog Test Blog");
        anotherBlogPostHelper.setContent("Another Test Blog Content");
        anotherBlogPostHelper.setCreationDate(new Date());

        assertTrue(anotherBlogPostHelper.create());

        try {
            updateBlog(anotherBlogPostHelper.getId(), "Test MetaWeblog Blog", "Edited Test Blog Content", null, true);
            fail("XmlRpcException expected to be thrown because there is a blog with the same title on the same day");
        } catch (final XmlRpcException xmlRpcE) {
            /* Success */
        }

        assertTrue(anotherBlogPostHelper.delete());
        assertTrue(blogPostHelper.delete());
    }

    public void testGetBlogPost() throws XmlRpcException, IOException {
        final Object blogId;
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        final Map blogStructure;
        final List blogLabels;

        blogId = createNewBlog("ds", "Test MetaWeblog Blog", "Test Blog Content", new String[] { "label01", "label02" }, true);

        blogStructure = (Map) xmlRpcClient.execute("metaWeblog.getPost",
                new Vector(
                        Arrays.asList(
                                new Object[] {
                                        blogId.toString(),
                                        getConfluenceWebTester().getCurrentUserName(),
                                        getConfluenceWebTester().getCurrentPassword(),
                                }
                        )
                ));

        assertNotNull(blogStructure);

        assertEquals(blogStructure.get("author"), getConfluenceWebTester().getCurrentUserName());
        assertEquals(blogStructure.get("title"), "Test MetaWeblog Blog");
        assertEquals(blogStructure.get("description"), "Test Blog Content");
        assertEquals(blogStructure.get("postid"), blogId.toString());
        
        assertEquals(simpleDateFormat.format(blogStructure.get("pubDate")), simpleDateFormat.format(new Date()));
        assertEquals(simpleDateFormat.format(blogStructure.get("dateCreated")), simpleDateFormat.format(new Date()));

        assertEquals(blogStructure.get("link"), getConfluenceWebTester().getBaseUrl() + "//display/ds/" + simpleDateFormat.format(new Date()) + "/" + "Test+MetaWeblog+Blog");
        assertEquals(blogStructure.get("permaLink"), getConfluenceWebTester().getBaseUrl() + "//display/ds/" + simpleDateFormat.format(new Date()) + "/" + "Test+MetaWeblog+Blog");
        assertEquals(blogStructure.get("blogid"), String.valueOf("ds"));


        blogLabels = (List) blogStructure.get("categories");

        assertNotNull(blogLabels);
        assertEquals(2, blogLabels.size());
        assertTrue(blogLabels.contains("label01"));
        assertTrue(blogLabels.contains("label02"));

        assertTrue(getBlogPostHelper(Long.parseLong(blogId.toString())).delete());
    }

    public void testGetCategories() throws XmlRpcException, IOException {
        final PageHelper pageHelper = getPageHelper();
        final PageHelper anotherPageHelper = getPageHelper();
        final SpaceHelper spaceHelper = getSpaceHelper();
        final List dsSpaceLabels;
        final Map categories;

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Confluence Overview");

        pageHelper.setId(pageHelper.findBySpaceKeyAndTitle());
        assertTrue(0 < pageHelper.getId());

        assertTrue(pageHelper.read());

        dsSpaceLabels = new ArrayList(21);
        for (int i = 1; i <= 21; ++i) {
            dsSpaceLabels.add("label" + i);
        }

        pageHelper.setLabels(dsSpaceLabels);
        /* Create 21 "global" labels */
        assertTrue(pageHelper.update());

        /* Create space for blogid */
        spaceHelper.setKey("tst");
        spaceHelper.setName("Test Space");
        spaceHelper.setDescription("Test Space");
        assertTrue(spaceHelper.create());

        anotherPageHelper.setSpaceKey(spaceHelper.getKey());
        anotherPageHelper.setTitle("Test Page");
        anotherPageHelper.setContent("Test Page Content");
        anotherPageHelper.setLabels(Arrays.asList(new String[] { "label20", "label19" }));
        assertTrue(anotherPageHelper.create());

        categories = (Map) xmlRpcClient.execute(
                "metaWeblog.getCategories",
                new Vector(
                        Arrays.asList(
                                new Object[] {
                                        "tst",
                                        getConfluenceWebTester().getCurrentUserName(),
                                        getConfluenceWebTester().getCurrentPassword(),
                                }
                        )
                )
        );

        assertNotNull(categories);
        assertEquals(20, categories.size());

        assertTrue(categories.keySet().containsAll(dsSpaceLabels.subList(0, 20)));
        assertTrue(categories.keySet().containsAll(anotherPageHelper.getLabels()));

        for (final Iterator i = categories.entrySet().iterator(); i.hasNext();) {
            final Map.Entry e = (Map.Entry) i.next();
            final String labelName = (String) e.getKey();
            final Map labelStructure = (Map) e.getValue();

            assertEquals(labelName, labelStructure.get("description"));
            assertNotNull(getConfluenceWebTester().getBaseUrl() + "/label/" + labelName, labelStructure.get("htmlUrl"));
            assertNotNull(labelStructure.get("rssUrl"));
        }

        /* Remove all the labels we have set */
        pageHelper.setLabels(Collections.EMPTY_LIST);
        assertTrue(pageHelper.update());

        assertTrue(spaceHelper.delete());
    }

    public void testGetRecentPostsLimitedByMaxPosts() throws XmlRpcException, IOException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        final List blogs;
        final Map blogStructure;
        final List blogLabels;

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test MetaWeblog Blog");
        blogPostHelper.setContent("Test Blog Content");
        blogPostHelper.setCreationDate(new Date());
        blogPostHelper.setLabels(Arrays.asList(new String[] { "recent-label-01", "recent-label-02" }));

        assertTrue(blogPostHelper.create());

        /* At this point, we will have two blogs (one of them is the Octagon blog post).
         * We want to test the remote methods ability to return a limited number of posts
         */
        blogs = (List) xmlRpcClient.execute(
                "metaWeblog.getRecentPosts",
                new Vector(
                        Arrays.asList(
                                new Object[] {
                                        "ds",
                                        getConfluenceWebTester().getAdminUserName(),
                                        getConfluenceWebTester().getAdminPassword(),
                                        new Integer(1)
                                }
                        )
                )
        );

        assertNotNull(blogs);
        assertEquals(1, blogs.size());

        blogStructure = (Map) blogs.get(0);

        assertEquals(blogStructure.get("author"), getConfluenceWebTester().getCurrentUserName());
        assertEquals(blogStructure.get("title"), "Test MetaWeblog Blog");
        assertEquals(blogStructure.get("description"), "<p>Test Blog Content</p>");
        assertEquals(blogStructure.get("postid"), String.valueOf(blogPostHelper.getId()));

        assertEquals(simpleDateFormat.format(blogStructure.get("pubDate")), simpleDateFormat.format(new Date()));
        assertEquals(simpleDateFormat.format(blogStructure.get("dateCreated")), simpleDateFormat.format(new Date()));

        assertEquals(blogStructure.get("link"), getConfluenceWebTester().getBaseUrl() + "//display/ds/" + simpleDateFormat.format(new Date()) + "/" + "Test+MetaWeblog+Blog");
        assertEquals(blogStructure.get("permaLink"), getConfluenceWebTester().getBaseUrl() + "//display/ds/" + simpleDateFormat.format(new Date()) + "/" + "Test+MetaWeblog+Blog");
        assertEquals(blogStructure.get("blogid"), String.valueOf("ds"));


        blogLabels = (List) blogStructure.get("categories");

        assertNotNull(blogLabels);
        assertEquals(2, blogLabels.size());
        assertTrue(blogLabels.contains("recent-label-01"));
        assertTrue(blogLabels.contains("recent-label-02"));

        assertTrue(blogPostHelper.delete());
    }
}
