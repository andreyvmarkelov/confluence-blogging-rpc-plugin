package it.com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.*;
import java.text.SimpleDateFormat;

import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;

public class BloggerApiTestCase extends AbstractBloggingRpcTestCase {

    private Object createNewBlog(final String spaceKey, final String content, final boolean publish) throws XmlRpcException, IOException {
        return xmlRpcClient.execute(
                "blogger.newPost",
                toXmlRpcParams(
                        new Object[] {
                                StringUtils.EMPTY,
                                spaceKey,
                                getConfluenceWebTester().getCurrentUserName(),
                                getConfluenceWebTester().getCurrentPassword(),
                                content,
                                publish ? Boolean.TRUE : Boolean.FALSE
                        }));
    }

    private Object updateBlog(final long blogPostId, final String content, final boolean publish) throws XmlRpcException, IOException {
        return xmlRpcClient.execute(
                "blogger.editPost",
                toXmlRpcParams(
                        new Object[] {
                                StringUtils.EMPTY,
                                String.valueOf(blogPostId),
                                getConfluenceWebTester().getCurrentUserName(),
                                getConfluenceWebTester().getCurrentPassword(),
                                content,
                                publish ? Boolean.TRUE : Boolean.FALSE
                        }));
    }

    private Object removeBlog(final long blogPostId, final boolean publish) throws XmlRpcException, IOException {
        return xmlRpcClient.execute(
                "blogger.deletePost",
                toXmlRpcParams(
                        new Object[] {
                                StringUtils.EMPTY,
                                String.valueOf(blogPostId),
                                getConfluenceWebTester().getCurrentUserName(),
                                getConfluenceWebTester().getCurrentPassword(),
                                publish ? Boolean.TRUE : Boolean.FALSE
                        }));
    }

    public void testCreateDraftBlog() throws IOException  {
        try {
            createNewBlog("ds", "<title>Test Blog</title>Test Blog Content.", false);
            fail("XmlRpcException should be thrown because draft blogs are not supported.");
        } catch (final XmlRpcException xmlRpcE) {
            /* Success */
        }
    }

    public void testUpdateDraftBlog() throws IOException  {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test Blog");
        blogPostHelper.setContent("Test Blog Content");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());

        try {
            updateBlog(blogPostHelper.getId(), "<title>Test Blog</title>Edited Test Blog Content", false);
            fail("XmlRpcException should be thrown because draft blogs are not supported.");
        } catch (final XmlRpcException xmlRpcE) {
            /* Success */
        }

        assertTrue(blogPostHelper.delete());
    }

    public void testCreateBlog() throws XmlRpcException, IOException {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        final Object blogId;

        blogId = createNewBlog("ds", "<title>Test Blog</title>Test Blog Content", true);

        gotoPage("/display/ds/" + simpleDateFormat.format(new Date()) + "/Test+Blog");
        assertTextPresent("Test Blog Content");

        removeBlog(Long.parseLong(blogId.toString()), true);
    }

    public void testCreateBlogWithoutTitle() throws XmlRpcException, IOException {
        try {
            createNewBlog("ds", "Test Blog Content", true);
            fail("XmlRpcException expected to be thrown because blog has no title (<title/>)");
        } catch (final XmlRpcException xmlRpcE) {
            /* Success */
        }
    }

    public void testCreateBlogWithDuplicateTitleInTheSamePublishedDay() throws XmlRpcException, IOException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test Blog");
        blogPostHelper.setContent("Test Blog Content");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());

        try {
            createNewBlog("ds", "<title>Test Blog</title>Test Blog Content", true);
            fail("XmlRpcException expected to be thrown because there is a blog with the same title on the same day");
        } catch (final XmlRpcException xmlRpcE) {
            /* Success */
        }

        assertTrue(blogPostHelper.delete());
    }

    public void testEditBlog() throws XmlRpcException, IOException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        final Object updateSuccessful;

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test Blog");
        blogPostHelper.setContent("Test Blog Content");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());

        updateSuccessful = updateBlog(blogPostHelper.getId(), "<title>Test Blog</title>Edited Test Blog Content", true);
        assertEquals(Boolean.TRUE, updateSuccessful);

        gotoPage("/display/ds/" + simpleDateFormat.format(new Date()) + "/Test+Blog");
        assertTextPresent("Edited Test Blog Content");

        assertTrue(blogPostHelper.delete());
    }

    public void testEditBlogWithoutTitle() throws XmlRpcException, IOException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test Blog");
        blogPostHelper.setContent("Test Blog Content");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());

        try {
            updateBlog(blogPostHelper.getId(), "Edited Test Blog Content", true);
            fail("XmlRpcException expected to be thrown because blog edits without title should not be allowed.");
        } catch (final XmlRpcException xmlRpcE) {
            /* Success */
        }

        assertTrue(blogPostHelper.delete());
    }

    public void testEditBlogWithDuplicateTitleInTheSamePublishedDay() throws XmlRpcException, IOException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final BlogPostHelper anotherBlogPostHelper = getBlogPostHelper();

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test Blog");
        blogPostHelper.setContent("Test Blog Content");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());

        /* Create another blog, the one to be edited */
        anotherBlogPostHelper.setSpaceKey("ds");
        anotherBlogPostHelper.setTitle("Another Test Blog");
        anotherBlogPostHelper.setContent("Another Test Blog Content");
        anotherBlogPostHelper.setCreationDate(new Date());

        assertTrue(anotherBlogPostHelper.create());

        try {
            updateBlog(anotherBlogPostHelper.getId(), "<title>Test Blog</title>Edited Test Blog Content", true);
            fail("XmlRpcException expected to be thrown because there is a blog with the same title on the same day");
        } catch (final XmlRpcException xmlRpcE) {
            /* Success */
        }

        assertTrue(anotherBlogPostHelper.delete());
        assertTrue(blogPostHelper.delete());
    }

    public void testGetBlogPost() throws XmlRpcException, IOException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        final Map blogStructure;

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test Blog");
        blogPostHelper.setContent("Test Blog Content");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());

        blogStructure = (Map) xmlRpcClient.execute("blogger.getPost",
                new Vector(
                        Arrays.asList(
                                new Object[] {
                                        StringUtils.EMPTY,
                                        String.valueOf(blogPostHelper.getId()),
                                        getConfluenceWebTester().getCurrentUserName(),
                                        getConfluenceWebTester().getCurrentPassword(),
                                }
                        )
                ));

        assertNotNull(blogStructure);

        assertEquals(blogStructure.get("postid"), String.valueOf(blogPostHelper.getId()));
        assertEquals(blogStructure.get("blogid"), String.valueOf(blogPostHelper.getSpaceKey()));
        assertEquals(blogStructure.get("url"), getConfluenceWebTester().getBaseUrl() + "/display/ds/" + simpleDateFormat.format(new Date()) + "/" + "Test+Blog");
        assertEquals(blogStructure.get("title"), blogPostHelper.getTitle());
        assertEquals(blogStructure.get("content"), "<title>" + blogPostHelper.getTitle() + "</title><p>" + blogPostHelper.getContent() + "</p>");
        assertEquals(blogStructure.get("authorName"), getConfluenceWebTester().getCurrentUserName());
        assertEquals(blogStructure.get("authorEmail"), "admin@confluence");
        assertEquals(simpleDateFormat.format(blogStructure.get("dateCreated")), simpleDateFormat.format(blogPostHelper.getCreationDate()));
        
        assertTrue(blogPostHelper.delete());
    }

    public void testGetUsersBlog() throws XmlRpcException, IOException {
        final SpaceHelper spaceHelper = getSpaceHelper();
        final UserHelper userHelper = getUserHelper();
        final List usersBlogs;
        final Map blog;

        /* Create a space which a user (created later) will not have access to */
        spaceHelper.setKey("foobar");
        spaceHelper.setName("Access to admin only space");
        spaceHelper.setDescription("Access to admin only space");

        assertTrue(spaceHelper.create());

        userHelper.setName("jane.doe");
        userHelper.setPassword("jane.doe");
        userHelper.setFullName("Jane Doe");
        userHelper.setEmailAddress("jane.doe@localhost.localdomain");
        userHelper.setGroups(Arrays.asList(new String[] { "confluence-users" }));

        assertTrue(userHelper.create());

        usersBlogs = (List) xmlRpcClient.execute(
                "blogger.getUsersBlogs",
                new Vector(
                        Arrays.asList(
                                new Object[] {
                                        StringUtils.EMPTY,
                                        "jane.doe",
                                        "jane.doe",
                                }
                        )
                )
        );

        assertNotNull(usersBlogs);
        assertEquals(1, usersBlogs.size());

        blog = (Map) usersBlogs.get(0);

        assertEquals("Demonstration Space", blog.get("blogName"));
        assertEquals(getConfluenceWebTester().getBaseUrl() + "/pages/viewrecentblogposts.action?key=ds", blog.get("url"));
        assertEquals("ds", blog.get("blogid"));

        assertTrue(userHelper.delete());
        assertTrue(spaceHelper.delete());
    }

    public void testGetUserInfo() throws XmlRpcException, IOException {
        final UserHelper userHelper = getUserHelper();
        final Map userInfo;

        userHelper.setName("jane.doe");
        userHelper.setPassword("jane.doe");
        userHelper.setFullName("Jane Doe");
        userHelper.setEmailAddress("jane.doe@localhost.localdomain");
        userHelper.setGroups(Arrays.asList(new String[] { "confluence-users" }));

        assertTrue(userHelper.create());

        userInfo = (Map) xmlRpcClient.execute(
                "blogger.getUserInfo",
                new Vector(
                        Arrays.asList(
                                new Object[] {
                                        StringUtils.EMPTY,
                                        "jane.doe",
                                        "jane.doe"
                                }
                        )
                )
        );

        assertNotNull(userInfo);

        assertEquals("Jane Doe", userInfo.get("firstname"));
        assertEquals("jane.doe@localhost.localdomain", userInfo.get("email"));
        assertEquals("jane.doe", userInfo.get("userid"));

        assertTrue(userHelper.delete());
    }
}
