package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SaveContext;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.AuthenticationFailedException;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Calendar;
import java.util.Date;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestBloggerEditPost extends TestCase
{
    @Mock
    private BloggingUtils bloggingUtils;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private SettingsManager settingsManager;

    @Mock
    private ConfluenceUser user;

    private BloggerImpl bloggerImpl;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        bloggerImpl = new BloggerImpl(bloggingUtils, new TransactionTemplate()
        {
            @Override
            public <T> T execute(TransactionCallback<T> tTransactionCallback)
            {
                return tTransactionCallback.doInTransaction();
            }
        }, spaceManager, spacePermissionManager, pageManager, permissionManager, settingsManager);

        when(bloggingUtils.getText(anyString(), Matchers.<String[]>anyObject())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return invocationOnMock.getArguments()[0].toString();
                    }
                }
        );
    }

    @Override
    protected void tearDown() throws Exception
    {
        bloggingUtils = null;
        spaceManager = null;
        spacePermissionManager = null;
        pageManager = null;
        permissionManager = null;
        settingsManager = null;
        user = null;
        super.tearDown();
    }

    public void testEditNonExistentPost() throws AuthenticationFailedException
    {
        when(bloggingUtils.authenticateUser(anyString(), anyString())).thenReturn(user);

        try
        {
            bloggerImpl.editPost("", String.valueOf(0), "", "", "", true);
            fail("RemoteException should've been raised");
        }
        catch (RemoteException re)
        {
            assertEquals("error.validation.blog.doesnotexists", re.getMessage());
        }
    }

    public void testEditRestrictedPost() throws AuthenticationFailedException
    {
        BlogPost post = new BlogPost();
        post.setSpace(new Space("TST"));

        when(bloggingUtils.authenticateUser(anyString(), anyString())).thenReturn(user);
        when(pageManager.getBlogPost(0L)).thenReturn(post);

        try
        {
            bloggerImpl.editPost("", String.valueOf(0), "", "", "", true);
            fail("RemoteException should've been raised");
        }
        catch (RemoteException re)
        {
            assertEquals("error.permission.edit.space.blog", re.getMessage());
        }
    }

    public void testEditPost() throws RemoteException
    {
        BlogPost post = new BlogPost();

        post.setSpace(new Space("TST"));
        post.setBodyAsString("");
        post.setCreationDate(new Date());

        final String title = "title";
        post.setTitle(title);

        when(bloggingUtils.authenticateUser(anyString(), anyString())).thenReturn(user);
        when(pageManager.getBlogPost(0L)).thenReturn(post);
        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, post.getSpace(), user)).thenReturn(true);

        bloggerImpl.editPost("", String.valueOf(0), "", "", "<title>" + title + " edited</title>content", true);

        verify(pageManager).saveContentEntity(argThat(
                new BaseMatcher<ContentEntityObject>()
                {
                    public boolean matches(Object o)
                    {
                        BlogPost blogPost = (BlogPost) o;
                        return StringUtils.equals(title + " edited", blogPost.getTitle())
                                && StringUtils.equals("content", blogPost.getBodyAsString());
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("New blog post not as expected");
                    }
                }
        ), Matchers.<ContentEntityObject>anyObject(), Matchers.<SaveContext>anyObject());
    }

    public void testRenamePostToSomethingElseWithTheSameTitle() throws RemoteException
    {
        BlogPost post = new BlogPost();

        post.setSpace(new Space("TST"));
        post.setBodyAsString("");
        post.setCreationDate(new Date());

        final String title = "title";
        post.setTitle(title);

        when(bloggingUtils.authenticateUser(anyString(), anyString())).thenReturn(user);
        when(pageManager.getBlogPost(0L)).thenReturn(post);
        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, post.getSpace(), user)).thenReturn(true);

        BlogPost conflictingPost = new BlogPost();
        conflictingPost.setTitle(title + " edited");
        when(pageManager.getBlogPost(eq(post.getSpaceKey()), eq(title + " edited"), Matchers.<Calendar>anyObject())).thenReturn(
                conflictingPost
        );

        try
        {
            bloggerImpl.editPost("", String.valueOf(0), "", "", "<title>" + title + " edited</title>content", true);
            fail("RemoteException should've been raised");
        }
        catch (RemoteException re)
        {
            assertEquals("error.validation.blog.duplicate", re.getMessage());
        }
    }
}