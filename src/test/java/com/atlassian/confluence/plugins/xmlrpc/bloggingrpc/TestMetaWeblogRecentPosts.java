package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.user.User;
import junit.framework.TestCase;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;


public class TestMetaWeblogRecentPosts extends TestCase
{
    @Mock
    private BloggingUtils bloggingUtils;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private LabelManager labelManager;

    @Mock
    private SettingsManager settingsManager;

    private MetaWeblogImpl metaWeblog;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        when(bloggingUtils.getText(anyString(), Matchers.<String[]>anyObject())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return invocationOnMock.getArguments()[0].toString();
                    }
                }
        );

        Settings globalSettings = new Settings();
        globalSettings.setBaseUrl("http://localhost:1990/confluence");
        when(settingsManager.getGlobalSettings()).thenReturn(globalSettings);

        metaWeblog = new MetaWeblogImpl(bloggingUtils, new TransactionTemplate()
        {
            @Override
            public <T> T execute(TransactionCallback<T> tTransactionCallback)
            {
                return tTransactionCallback.doInTransaction();
            }
        }, spaceManager, spacePermissionManager, pageManager, permissionManager, labelManager, settingsManager);
    }

    @Override
    protected void tearDown() throws Exception
    {
        bloggingUtils = null;
        spaceManager = null;
        spacePermissionManager = null;
        pageManager = null;
        permissionManager = null;
        labelManager = null;
        settingsManager = null;
        super.tearDown();
    }

    public void testGetRecentPostsFromNonExistentSpace()
    {
        try
        {
            metaWeblog.getRecentPosts("", "", "", 10);
            fail("RemoteException should've been raised");
        }
        catch (RemoteException re)
        {
            assertEquals("error.validation.space.unknown", re.getMessage());
        }
    }

    public void testGetRecentPostsFromRestricedSpace()
    {
        String spaceKey = "TST";
        Space space = new Space(spaceKey);

        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        try
        {
            metaWeblog.getRecentPosts(spaceKey, "", "", 10);
            fail("RemoteException should've been raised");
        }
        catch (RemoteException re)
        {
            assertEquals("error.permission.view.space.blog", re.getMessage());
        }
    }

    public void testZeroRecentPosts() throws RemoteException
    {
        String spaceKey = "TST";
        Space space = new Space(spaceKey);

        when(spaceManager.getSpace(spaceKey)).thenReturn(space);
        when(spacePermissionManager.hasPermission(eq(SpacePermission.VIEWSPACE_PERMISSION), eq(space), Matchers.<User>anyObject())).thenReturn(true);

        assertEquals(new Vector(), metaWeblog.getRecentPosts(spaceKey, "", "", 0));
    }

    public void testRecentPosts() throws RemoteException
    {
        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        int noOfPosts = 10;

        BlogPost recentPost = new BlogPost();
        recentPost.setSpace(space);
        recentPost.setTitle("Title");
        recentPost.setBodyAsString("Content");
        recentPost.setCreationDate(new Date());

        when(spaceManager.getSpace(spaceKey)).thenReturn(space);
        when(spacePermissionManager.hasPermission(eq(SpacePermission.VIEWSPACE_PERMISSION), eq(space), Matchers.<User>anyObject())).thenReturn(true);
        when(pageManager.getRecentlyAddedBlogPosts(noOfPosts, space.getKey())).thenReturn(
                Arrays.asList(recentPost)
        );
         when(bloggingUtils.convertStorageFormatToView(recentPost)).thenReturn(recentPost.getBodyAsString());

        Vector posts = metaWeblog.getRecentPosts(spaceKey, "", "", noOfPosts);
        assertNotNull(posts);
        assertEquals(1, posts.size());

        Hashtable post = (Hashtable) posts.get(0);
        assertEquals(recentPost.getIdAsString(), post.get(MetaWeblogImpl.POSTID));
        assertEquals(recentPost.getTitle(), post.get(MetaWeblogImpl.TITLE));
        assertEquals(recentPost.getBodyAsString(), post.get(MetaWeblogImpl.DESCRIPTION));
    }
}