package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class TestMetaWeblogNewMedia extends TestCase
{
    @Mock
    private BloggingUtils bloggingUtils;

    @Mock
    private TransactionTemplate transactionTemplate;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private LabelManager labelManager;

    @Mock
    private SettingsManager settingsManager;

    private MetaWeblogImpl metaWeblog;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        metaWeblog = new MetaWeblogImpl(bloggingUtils, transactionTemplate, spaceManager, spacePermissionManager, pageManager, permissionManager, labelManager, settingsManager);
    }

    @Override
    protected void tearDown() throws Exception
    {
        bloggingUtils = null;
        transactionTemplate = null;
        spaceManager = null;
        spacePermissionManager = null;
        pageManager = null;
        permissionManager = null;
        labelManager = null;
        settingsManager = null;
        super.tearDown();
    }

    public void testNewMediaNotSupported() throws RemoteException
    {
        try
        {
            metaWeblog.newMediaObject(String.valueOf(0), "", "", null);
            fail("NotImplementedException should've been raised");
        }
        catch (NotImplementedException notImplemented)
        {

        }
    }
}