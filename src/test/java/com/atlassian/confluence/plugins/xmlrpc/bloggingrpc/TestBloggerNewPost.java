package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SaveContext;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Calendar;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestBloggerNewPost extends TestCase
{
    @Mock
    private BloggingUtils bloggingUtils;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private SettingsManager settingsManager;

    @Mock
    private ConfluenceUser user;

    private BloggerImpl bloggerImpl;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        bloggerImpl = new BloggerImpl(bloggingUtils, new TransactionTemplate()
        {
            @Override
            public <T> T execute(TransactionCallback<T> tTransactionCallback)
            {
                return tTransactionCallback.doInTransaction();
            }
        }, spaceManager, spacePermissionManager, pageManager, permissionManager, settingsManager);

        when(bloggingUtils.getText(anyString(), Matchers.<String[]>anyObject())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return invocationOnMock.getArguments()[0].toString();
                    }
                }
        );

        when(bloggingUtils.getText(anyString())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return invocationOnMock.getArguments()[0].toString();
                    }
                }
        );
    }

    @Override
    protected void tearDown() throws Exception
    {
        bloggingUtils = null;
        spaceManager = null;
        spacePermissionManager = null;
        pageManager = null;
        permissionManager = null;
        settingsManager = null;
        user = null;
        super.tearDown();
    }

    public void testPostInNonExistentSpace()
    {
        try
        {
            bloggerImpl.newPost("", "TST", "", "", "", true);
            fail("RemoteException should've been raised");
        }
        catch (RemoteException re)
        {
            assertEquals("error.validation.space.unknown", re.getMessage());
        }
    }

    public void testPostWithoutSufficientPrivileges()
    {
        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        try
        {
            bloggerImpl.newPost("", spaceKey, "", "", "", true);
            fail("RemoteException should've been raised");
        }
        catch (RemoteException re)
        {
            assertEquals("error.permission.edit.space.blog", re.getMessage());
        }
    }

    public void testPostWithoutTitle() throws RemoteException
    {
        String userName = "john.doe";
        String password = "";
        when(bloggingUtils.authenticateUser(userName, password)).thenReturn(user);

        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user)).thenReturn(true);

        try
        {
            bloggerImpl.newPost("", spaceKey, userName, password, "Content", true);
            fail("RemoteException should've been raised");
        }
        catch (RemoteException re)
        {
            assertEquals("error.validation.blog.title.blank", re.getMessage());
        }
    }

    public void testCreateDuplicateBlogPost() throws RemoteException
    {
        String userName = "john.doe";
        String password = "";
        when(bloggingUtils.authenticateUser(userName, password)).thenReturn(user);

        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user)).thenReturn(true);
        when(pageManager.getBlogPost(eq(space.getKey()), eq("Title"), Matchers.<Calendar>anyObject())).thenReturn(new BlogPost());

        try
        {
            bloggerImpl.newPost("", spaceKey, userName, password, "<title>Title</title>Content", true);
            fail("RemoteException should've been raised");
        }
        catch (RemoteException re)
        {
            assertEquals("error.validation.blog.duplicate", re.getMessage());
        }
    }

    public void testPostWithSufficientPrivileges() throws RemoteException
    {
        String userName = "john.doe";
        String password = "";
        when(bloggingUtils.authenticateUser(userName, password)).thenReturn(user);

        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user)).thenReturn(true);

        bloggerImpl.newPost("", spaceKey, userName, password, "<title>Title</title>Content", true);
        verify(pageManager).saveContentEntity(argThat(
                new BaseMatcher<ContentEntityObject>()
                {
                    public boolean matches(Object o)
                    {
                        BlogPost post = (BlogPost) o;
                        return StringUtils.equals("Title", post.getTitle())
                                && StringUtils.equals("Content", post.getBodyAsString());
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not a blog post with title \"Title\" and content \"Content\"");
                    }
                }
        ), eq((SaveContext) null));
    }
}