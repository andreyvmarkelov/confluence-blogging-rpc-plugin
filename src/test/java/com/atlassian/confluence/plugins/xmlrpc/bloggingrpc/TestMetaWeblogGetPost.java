package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.user.User;
import junit.framework.TestCase;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Date;
import java.util.Hashtable;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

public class TestMetaWeblogGetPost extends TestCase
{
    @Mock
    private BloggingUtils bloggingUtils;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private LabelManager labelManager;

    @Mock
    private SettingsManager settingsManager;

    private MetaWeblogImpl metaWeblog;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        when(bloggingUtils.getText(anyString(), Matchers.<String[]>anyObject())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return invocationOnMock.getArguments()[0].toString();
                    }
                }
        );

        when(bloggingUtils.getText(anyString())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return invocationOnMock.getArguments()[0].toString();
                    }
                }
        );

        Settings globalSettings = new Settings();
        globalSettings.setBaseUrl("http://localhost:1990/confluence");
        when(settingsManager.getGlobalSettings()).thenReturn(globalSettings);

        metaWeblog = new MetaWeblogImpl(bloggingUtils, new TransactionTemplate()
        {
            @Override
            public <T> T execute(TransactionCallback<T> tTransactionCallback)
            {
                return tTransactionCallback.doInTransaction();
            }
        }, spaceManager, spacePermissionManager, pageManager, permissionManager, labelManager, settingsManager);
    }

    @Override
    protected void tearDown() throws Exception
    {
        bloggingUtils = null;
        spaceManager = null;
        spacePermissionManager = null;
        pageManager = null;
        permissionManager = null;
        labelManager = null;
        settingsManager = null;
        super.tearDown();
    }

    public void testGetPostWithNonNumericId()
    {
        try
        {
            metaWeblog.getPost("foobar", "", "");
            fail("RemoteException should've been raised");
        }
        catch (RemoteException re)
        {
            assertEquals("error.validation.blog.invalidpostid", re.getMessage());
        }
    }

    public void testGetPostWithInvalidId()
    {
        try
        {
            metaWeblog.getPost(String.valueOf(0), "", "");
            fail("RemoteException should've been raised");
        }
        catch (RemoteException re)
        {
            assertEquals("error.validation.blog.doesnotexists", re.getMessage());
        }
    }

    public void testGetRestrictedPost()
    {
        when(pageManager.getBlogPost(0L)).thenReturn(new BlogPost());

        try
        {
            metaWeblog.getPost(String.valueOf(0), "", "");
            fail("RemoteException should've been raised");
        }
        catch (RemoteException re)
        {
            assertEquals("error.permission.view.blog", re.getMessage());
        }
    }

    public void testGetPost() throws RemoteException
    {
        String spaceKey = "TST";
        Space space = new Space(spaceKey);

        BlogPost bp = new BlogPost();
        bp.setSpace(space);
        bp.setTitle("foobar");
        bp.setBodyAsString("barfoo");

        long postId = 1;
        bp.setId(postId);

        Date now = new Date();
        bp.setCreationDate(now);

        when(pageManager.getBlogPost(postId)).thenReturn(bp);
        when(permissionManager.hasPermission(Matchers.<User>anyObject(), eq(Permission.VIEW), eq(bp))).thenReturn(true);
        when(bloggingUtils.convertStorageFormatToView(bp)).thenReturn(bp.getBodyAsString());


        Hashtable post = metaWeblog.getPost(String.valueOf(postId), "", "");
        assertNotNull(post);

        assertEquals(bp.getTitle(), post.get(MetaWeblogImpl.TITLE));
        assertEquals(bp.getBodyAsString(), post.get(MetaWeblogImpl.DESCRIPTION));
        assertEquals(bp.getIdAsString(), post.get(MetaWeblogImpl.POSTID));
    }
}