package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import junit.framework.TestCase;

import java.util.HashMap;

public class BloggerImplTest extends TestCase
{
    private BloggerImpl blogger;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        blogger = new BloggerImpl(null, null, null, null, null, null, null);
    }

    public void testSplitContent()
    {
        assertEquals(
                new HashMap<String, String>()
                {
                    {
                        put(BloggerImpl.CONTENT, "contents");
                        put(BloggerImpl.TITLE, "title");
                    }
                },
                blogger.splitBlogContent("<title>title</title>contents"));
    }

    public void testSplitTitleTrimmedButNotContent()
    {
        assertEquals(
                new HashMap<String, String>()
                {
                    {
                        put(BloggerImpl.CONTENT, "\n contents");
                        put(BloggerImpl.TITLE, "title");
                    }
                },
                blogger.splitBlogContent("  \n  <title>title</title>\n contents"));
    }

    public void testSplitWithMultipleTitleTags()
    {
        assertEquals(
                new HashMap<String, String>()
                {
                    {
                        put(BloggerImpl.CONTENT, " some content <title>title 2</title> more content");
                        put(BloggerImpl.TITLE, "title one");
                    }
                },
                blogger.splitBlogContent("<title>title one</title> some content <title>title 2</title> more content"));
    }

    public void testSplitWithUnclosedTitleTag()
    {
        assertEquals(
                new HashMap<String, String>()
                {
                    {
                        put(BloggerImpl.CONTENT, "<title>invalid title<br> contents");
                    }
                },
                blogger.splitBlogContent("<title>invalid title<br> contents"));
    }

    public void testSplitWithMultipleUnclosedTitleTags()
    {
        assertEquals(
                new HashMap<String, String>()
                {
                    {
                        put(BloggerImpl.CONTENT, "<title>invalid title<title> contents");
                    }
                },
                blogger.splitBlogContent("<title>invalid title<title> contents"));
    }

    public void testSplitWithoutContent()
    {
        assertEquals(
                new HashMap<String, String>()
                {
                    {
                        put(BloggerImpl.TITLE, "title");
                        put(BloggerImpl.CONTENT, "");
                    }
                },
                blogger.splitBlogContent("<title>title</title>"));
    }

    public void testSplitWithBlank()
    {
        assertEquals(
                new HashMap<String, String>()
                {
                    {
                        put(BloggerImpl.CONTENT, "");
                    }
                },
                blogger.splitBlogContent(""));
    }
}